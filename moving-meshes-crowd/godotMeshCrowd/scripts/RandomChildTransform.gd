extends Node3D

var timer = 0.

func _process(delta):
	timer = timer + 0.1
	for child in get_children():
		child.translate(Vector3(sin(timer) * randf(), sin(timer) * randf(), 0.))
