﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stride.Core.Mathematics;
using Stride.Input;
using Stride.Engine;
using Stride.Games;
using Stride.Graphics;

namespace StrideMeshCrowd
{
    public class RandomChildTransform : SyncScript
    {
        private float counter = 0;
        private Random rand;

        public override void Start()
        {
            rand = new Random();
            GraphicsDevice.Presenter.PresentInterval = PresentInterval.Immediate;
        }

        public override void Update()
        {
            DebugText.Print(Game.UpdateTime.FramePerSecond.ToString(), new Int2(x: 50, y: 50));
            counter += 0.05f;
            foreach (var child in Entity.GetChildren())
            {
                var newPos = child.Transform.Position;
                newPos.Y += (float) (Math.Sin(counter) * rand.NextDouble());
                newPos.Z += (float) (Math.Sin(counter) * rand.NextDouble());
                child.Transform.Position = newPos;
            }
        }
    }
}
