using Stride.Engine;

namespace StrideMeshCrowd
{
    class StrideMeshCrowdApp
    {
        static void Main(string[] args)
        {
            using (var game = new Game())
            {
                game.Run();
            }
        }
    }
}
