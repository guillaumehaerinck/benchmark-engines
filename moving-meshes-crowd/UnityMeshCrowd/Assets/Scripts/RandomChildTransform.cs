﻿using System;
using UnityEngine;

public class RandomChildTransform : MonoBehaviour
{
    void Update()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var newPos = transform.GetChild(i).position;
            newPos.x += (float) Math.Sin(Time.time) * UnityEngine.Random.value * 0.04f;
            newPos.y += (float) Math.Sin(Time.time) * UnityEngine.Random.value * 0.04f;
            transform.GetChild(i).position = newPos;
        }
    }
}
