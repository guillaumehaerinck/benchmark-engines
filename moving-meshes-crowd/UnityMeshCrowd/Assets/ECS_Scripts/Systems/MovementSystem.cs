﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Jobs;

public class MovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        float time = (float) Time.ElapsedTime;
        var rand = new Random((uint) time);

        Entities.ForEach((ref Translation trans) =>
        {
            trans.Value += new float3(math.sin(time) * rand.NextFloat(), math.sin(time) * rand.NextFloat(), 0) * 0.1f;
        })
        .ScheduleParallel();
    }
}
