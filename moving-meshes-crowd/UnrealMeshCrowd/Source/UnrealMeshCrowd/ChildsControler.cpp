// Fill out your copyright notice in the Description page of Project Settings.


#include "ChildsControler.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AChildsControler::AChildsControler()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AChildsControler::BeginPlay()
{
	Super::BeginPlay();
	GetAttachedActors(Childs);
}

// Called every frame
void AChildsControler::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	for (auto actor : Childs)
	{
		auto newPos = actor->GetActorLocation();
		newPos.X += sinf(GetGameTimeSinceCreation()) * FMath::FRand();
		newPos.Y += sinf(GetGameTimeSinceCreation()) * FMath::FRand();
		actor->SetActorLocation(newPos);
	}
}

