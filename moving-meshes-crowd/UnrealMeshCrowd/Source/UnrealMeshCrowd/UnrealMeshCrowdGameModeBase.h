// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealMeshCrowdGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALMESHCROWD_API AUnrealMeshCrowdGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
