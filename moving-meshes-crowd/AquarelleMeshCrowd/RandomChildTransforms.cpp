static float timer = 0.;

void RandomChildTransforms::Update()
{
	timer += 0.1;
	for (auto child : sceneObject->children) 
	{
		auto newPos = child->position;
		newPos.x += sin(timer * std::rand());
		newPos.y += sin(timer * std::rand());
		child->position = newPos;
	}
}
