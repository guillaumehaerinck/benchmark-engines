# Benchmark Game Engines

Set of test projects to compare raw performance of available Game Engines

## Test Suites

### Moving Meshes Crowd

Display many similar meshes on screen to check for instanced rendering, frustrum culling and data cache coherency for the engine iteration
